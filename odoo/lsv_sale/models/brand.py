from datetime import timedelta, time
from odoo import api, fields, models, _
from odoo.tools.float_utils import float_round

class Brand(models.Model):
    _name = "lsv.sale.brand"
    
    name = fields.Char(string='Name', required=True)
    status = fields.Boolean(default=True)


    department_id = fields.Many2one('sale.departament', string = 'Departamento', required=True)
    product_ids = fields.One2many('product.template',inverse_name='brand_id', string='Tipo producto')

