from datetime import timedelta, time
from odoo import api, fields, models, _
from odoo.tools.float_utils import float_round

class Product(models.Model):
    _inherit = "product.template"
    _name = 'product.template'

    name = fields.Char(string="Name", required=True)
    brand_id = fields.Many2one('lsv.sale.brand', string='Marca')
    department_id = fields.Many2one('sale.departament', string='Departamento', compute='_compute_department_id')

    @api.depends('brand_id')
    def _compute_department_id(self):
        for item in self:
            item.department_id = item.brand_id.department_id.id