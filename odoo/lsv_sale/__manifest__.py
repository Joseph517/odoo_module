# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'lsv sale extended',
    'version': '1.0',
    'category': 'Sales/Sales',
    'sequence': 17,
    'summary': 'From quotations to invoices',
    'depends': ['sale','purchase'],
    'data': [
    'security/user_group.xml',
    'security/ir.model.access.csv',
    'views/product.xml',
    'views/department.xml',
    'data/department_data.xml',
    'views/brand.xml',
    ],
    'demo': [],
    'application': True,
}
