from datetime import timedelta, time
from odoo import api, fields, models, _
from odoo.tools.float_utils import float_round

class SaleDepartment(models.Model):

    _name = "sale.departament"
    
    name = fields.Char(string='Nombre', required=True)
    status = fields.Boolean(default=True)
    discount = fields.Float(string='Descuento')
    brand_ids = fields.One2many('lsv.sale.brand',inverse_name='department_id', string='Marca')